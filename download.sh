#!/bin/bash
if [ ! -d arvindemnlp2014 ]; then

	mkdir arvindemnlp2014
	cd arvindemnlp2014
	wget http://iesl.cs.umass.edu/downloads/vectors/release.tar.gz
	wget https://bitbucket.org/jeevan_shankar/multi-sense-skipgram/get/74aeafd22528.zip
	unzip 74aeafd22528.zip
	tar -zxvf release.tar.gz
	rm -rf release.tar.gz
	rm -rf 74aeafd22528.zip
fi
if [ ! -d trunk ]; then
	svn checkout http://word2vec.googlecode.com/svn/trunk/
fi
if [ ! -d is13 ];then
	git clone git@github.com:mesnilgr/is13.git
fi
if [ ! -d word2vecf ];then
        hg clone https://bitbucket.org/yoavgo/word2vecf
fi
